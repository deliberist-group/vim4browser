"use strict";

/* eslint no-console: "off" */

const fs = require("fs");
const path = require("path");

const ConcatPlugin = require("webpack-concat-plugin");

const outputDir = path.resolve(__dirname, "dist/");

const deleteFolderRecursive = (name) => {
  if (fs.existsSync(name)) {
    fs.readdirSync(name).forEach((file, ignoredIndex) => {
      const curPath = path.join(name, file);
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        console.log(`Unlinking ${curPath}`);
        fs.unlinkSync(curPath);
      }
    });
    console.log(`Removing ${name}`);
    fs.rmdirSync(name);
  }
};

deleteFolderRecursive(outputDir);

/**
 * List all of the files that make up the meat of this project, but list them
 * carefully so that functions are defined before they can be used!
 */
const files = [
  path.resolve(__dirname, "lib/actions/scrolling.js"),
  path.resolve(__dirname, "lib/actions/windows.js"),
  path.resolve(__dirname, "lib/utils/queue.js"),
  path.resolve(__dirname, "lib/utils/command_processor.js"),
];

/**
 * "Browser Files" refers to all of the files needed to build a single
 * browser-imporable script.
 */
const browserFiles = files.concat([path.resolve(__dirname, "lib/browser.js")]);

/**
 * "User Script Files" refers to all of the files needed to build a single
 * user script that can be installed into something like TamperMonkey.  This
 * should build off of the "Browser Files."
 */
const userscriptFiles = [
  path.resolve(__dirname, "lib/user.js"),
].concat(browserFiles);

const commonConcatPlugin = {
  "attributes": {
    "async": true
  },
  "fileName": "[name].js",
  "outputPath": "",
  "sourceMap": false,
  "uglify": false,
};

module.exports = {
  "entry": {
    "bundle": browserFiles,
  },
  "mode": "none",
  "output": {
    "filename": "vim4browser.[name].js",
    "path": outputDir,
  },
  "plugins": [
    new ConcatPlugin(Object.assign(commonConcatPlugin, {
      "filesToConcat": browserFiles,
      "name": "vim4browser",
    })),
    new ConcatPlugin(Object.assign(commonConcatPlugin, {
      "filesToConcat": userscriptFiles,
      "name": "vim4browser.user",
    })),
  ],
  "target": "node",
};
