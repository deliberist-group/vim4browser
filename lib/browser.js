"use strict";

const KEY_HELD_DELAY_MS = 100;

const handleEvent = (event, processor) => {
  const {key} = event;
  processor.updateState(key);
};

(function main () {
  // Browser startup here...

  // Does this work up here?  Or does this need to be global?
  const processor = new CommandProcessor();

  // Handle the event "immediately" as the key is no longer pressed.
  document.addEventListener("keyup", (event) => {
    setTimeout(handleEvent, 0, event, processor);
  });

  // Handle the case where a key is HELD down.  But slow it down!
  document.addEventListener("keydown", (event) => {
    if (event.repeat) {
      setTimeout(handleEvent, KEY_HELD_DELAY_MS, event, processor);
    }
  });
}());
