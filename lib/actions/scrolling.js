"use strict";

const defaultScrollScalar = 20;

const defaultScrollFactor = 1;
const largeScrollFactor = 10;

const scrollLeft = (keyState, factor = defaultScrollFactor) => {
  // TODO regex the keyState searching for preceding numbers
  window.scrollBy(-defaultScrollScalar * factor, 0);
};

const scrollDown = (keyState, factor = defaultScrollFactor) => {
  // TODO regex the keyState searching for preceding numbers
  window.scrollBy(0, defaultScrollScalar * factor);
};

const scrollUp = (keyState, factor = defaultScrollFactor) => {
  // TODO regex the keyState searching for preceding numbers
  window.scrollBy(0, -defaultScrollScalar * factor);
};

const scrollRight = (keyState, factor = defaultScrollFactor) => {
  // TODO regex the keyState searching for preceding numbers
  window.scrollBy(defaultScrollScalar * factor, 0);
};

const scrollLeftLarge = (keyState) => {
  scrollLeft("", largeScrollFactor);
};

const scrollDownLarge = (keyState) => {
  scrollDown("", largeScrollFactor);
};

const scrollUpLarge = (keyState) => {
  scrollUp("", largeScrollFactor);
};

const scrollRightLarge = (keyState) => {
  scrollRight("", largeScrollFactor);
};
