"use strict";

const closeTab = (_) => {
  /* eslint no-alert: "off" */
  if (window.opener) {
    if (confirm("Close Window?")) {
      window.close();
    }
  } else {
    window.alert(
      "Unfortunately Javascript cannot close this tab/window. ¯\\_(ツ)_/¯"
    );
  }
};

const reloadPage = (_) => {
  location.reload();
};
