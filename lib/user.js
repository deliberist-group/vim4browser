// ==UserScript==
// @name        vim4browser.user.js
// @namespace   https://gitlab.com/deliberist/vim4browser
// @version     0.0.2
// @description Bringing Vim muscle memory to your browser or website!
// @author      rbprogrammer at gmail dot com
// @match       https://*/*
// @grant       none
// @updateURL   http://localhost:8080/vim4browser.user.js
// @downloadURL http://localhost:8080/vim4browser.user.js
// ==/UserScript==

/* eslint-env greasemonkey */
