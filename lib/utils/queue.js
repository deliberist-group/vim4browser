"use strict";

/* eslint max-classes-per-file: ["error", 2] */

class Queue extends Array {
  enqueue (val) {
    this.push(val);
  }

  dequeue () {
    return this.shift();
  }

  peek () {
    return this[0];
  }

  isEmpty () {
    return this.length === 0;
  }

  clear () {
    while (this.length) {
      this.pop();
    }
  }

  toString (delimiter = "") {
    let str = "";
    for (const value of this) {
      str += delimiter;
      str += value;
    }
    // Trim the leading delimiter!
    return str.substring(delimiter.length);
  }
}

const DEFAULT_MAX_SIZE = 10;

class BoundedQueue extends Queue {

  constructor (maxsize = DEFAULT_MAX_SIZE) {
    super();
    this.maxsize = maxsize;
  }

  enqueue (val) {
    let old = null;
    if (this.length === this.maxsize) {
      old = this.dequeue();
    }
    super.enqueue(val);
    return old;
  }
}
