"use strict";

/* eslint sort-keys: "off" */

/**
 * Each action should be a method/function that takes a single argument, which
 * will be the state of the keystrokes entered.  This is particularly useful for
 * regex commands that incorporate a number, like "5j" or "10k".
 *
 * For any action that starts with a colon (i.e. ":") then it must be terminated
 * with an "Enter" before keystrokes are matched with the action.  Otherwise,
 * the action is processed in real time.
 */
const DEFAULT_ACTIONS = {
  // Navigation!
  "h$": scrollLeft,
  "j$": scrollDown,
  "k$": scrollUp,
  "l$": scrollRight,
  "H$": scrollLeftLarge,
  "J$": scrollDownLarge,
  "K$": scrollUpLarge,
  "L$": scrollRightLarge,

  // Window controls.
  ":e$": reloadPage,
  ":q$": closeTab,
};

const DEFAULT_IGNORABLE_KEYS = [
  "Alt",
  "Control",
  "Shift",
];

class CommandProcessor {
  constructor (actions = DEFAULT_ACTIONS,
               ignorableKeys = DEFAULT_IGNORABLE_KEYS) {
    this.history = new BoundedQueue();
    this.ignorableKeys = Object.assign(ignorableKeys);
    this.waitForEnter = false;

    // Boy it would be fucking nice if we could pre-compile the regexs.  But
    // for some god forsaken reason (at least) Firefox fails to match anything.
    // It must be botching the regex object for every call to either:
    //  - string.match(regex)
    //  - regex.test(str)
    // For the time being... Just make a straight copy of the actions and
    // compile the regex for each keypress.
    //
    // this.actions = {};
    // for (const [command, action] of Object.entries(actions)) {
    //   const commandRegex = new RegExp(`${command}$`, "u");
    //   this.actions[commandRegex] = action;
    // }
    this.actions = Object.assign(actions);

  }

  clearState (keyState) {
    this.history.clear();
    this.waitForEnter = false;
  }

  updateState (key) {
    // Do not allow ignorable keys to effect the state.
    if (this.ignorableKeys.includes(key)) {
      console.log(`Ignoring key: ${key}`);
      return;
    }

    // Of course Escape takes precedence over everything and clears state.
    if (key === "Escape") {
      console.log("Clearing state!");
      this.clearState();
      return;
    }

    // If a colon was pressed then we need to wait for an Enter before the
    // command can be processed.
    //
    // I'm kind of thinking a State Machine would be a better design here...
    console.log(`Accepting key: ${key}`);
    if (key === ":") {
      this.waitForEnter = true;
      this.history.push(key);

    } else if (key === "Enter") {
      if (this.waitForEnter) {
        this.waitForEnter = false;
        this.processState();
      } else {
        console.log("Ignoring superfluous Enter.");
      }

    } else if (this.waitForEnter) {
      this.history.push(key);

    } else {
      this.history.push(key);
      this.processState();
    }
  }

  processState () {
    const state = this.history.toString();
    console.log(`Processing state = ${state}`);
    for (const [commandString, action] of Object.entries(this.actions)) {
      const commandRegex = new RegExp(commandString, "u");
      if (commandRegex.test(state)) {
        console.log(`Matched ${state} with ${commandRegex}.`);
        action(state);
        this.clearState();
        return;
      }
    }
  }
}
