# vim4browser

<img src="https://gitlab.com/deliberist/vim4browser/-/raw/main/resources/vim4browser.png"
     alt="vim4browser logo"
     width="250"
     height="250" />

[![pipeline](https://gitlab.com/deliberist/vim4browser/badges/main/pipeline.svg)](https://gitlab.com/deliberist/vim4browser/-/commits/main)
[![codecov](https://codecov.io/gl/deliberist/vim4browser/branch/main/graph/badge.svg)](https://codecov.io/gl/deliberist/vim4browser)
[![npm](https://img.shields.io/npm/dw/vim4browser.svg)](https://www.npmjs.com/package/vim4browser)
[![npm version](https://badge.fury.io/js/vim4browser.svg)](https://badge.fury.io/js/vim4browser)

<!--
[![Build status](https://ci.appveyor.com/api/projects/status/2w5wq7b1b9aa16eq?svg=true)](https://ci.appveyor.com/project/rbprogrammer/vim4browser)
-->

Bringing Vim muscle memory to your browser or website!

## Usage

- TODO how to install.
- TODO how to register custom keybindings.
- TODO how to use in the browser.

## Building

- TODO build for local use, like testing vim4browser in ViolentMonkey.
- TODO build for publishing
